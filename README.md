# 69Percent
This is a funny keyboard layout.
![Image of the layout](https://gitlab.com/justlike1984/69percent/-/raw/main/69Percent.png)
## Contributing
All contributions require the following:
- A change to the layout
- The benefits of the change

## Authors and acknowledgment
Original layout by justlike1984, with currently no contributions.
## License
Licensed under BSD License 2.0
